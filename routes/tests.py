from django.test import TestCase
from django.urls import reverse
from cities.models import City
from trains.models import Train
from django.core.exceptions import ValidationError
from routes import views as routes_views
from cities import views as cities_views
from routes.forms import RouteForm


class RoutesTestCase(TestCase):
    def setUp(self):
        self.city_A = City.objects.create(name='A')
        self.city_B = City.objects.create(name='B')
        self.city_C = City.objects.create(name='C')
        self.city_D = City.objects.create(name='D')
        self.city_E = City.objects.create(name='E')
        t1 = Train(number='t1', from_city=self.city_A, to_city=self.city_B, travel_time=9)
        t1.save()
        t2 = Train(number='t2', from_city=self.city_A, to_city=self.city_C, travel_time=7)
        t2.save()
        t3 = Train(number='t3', from_city=self.city_A, to_city=self.city_C, travel_time=10)
        t3.save()
        t4 = Train(number='t4', from_city=self.city_B, to_city=self.city_A, travel_time=11)
        t4.save()
        t5 = Train(number='t5', from_city=self.city_B, to_city=self.city_D, travel_time=8)
        t5.save()
        t6 = Train(number='t6', from_city=self.city_B, to_city=self.city_E, travel_time=3)
        t6.save()
        t7 = Train(number='t7', from_city=self.city_C, to_city=self.city_B, travel_time=6)
        t7.save()
        t8 = Train(number='t8', from_city=self.city_D, to_city=self.city_E, travel_time=4)
        t8.save()
        t9 = Train(number='t9', from_city=self.city_E, to_city=self.city_D, travel_time=5)
        t9.save()


    def test_model_city_duplicate(self):
        try:
            a_city = City(name='A')
            a_city.full_clean()
        except ValidationError as e:
            self.assertEqual({'name': ['City with this City already exists.']}, e.message_dict)


    def test_model_train_duplicate(self):
        try:
            train = Train(number='t2', from_city=self.city_A, to_city=self.city_C, travel_time=8)
            train.full_clean()
        except ValidationError as e:
            self.assertEqual({'number': ['Train with this Train number already exists.']}, e.message_dict)
        try:
            train = Train(number='t12', from_city=self.city_A, to_city=self.city_C, travel_time=7)
            train.full_clean()
        except ValidationError as e:
            self.assertEqual({'__all__': ['Change travel time, please!']}, e.message_dict)

    def test_home_routes_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, template_name='routes/home.html')
        self.assertEqual(routes_views.home, response.resolver_match.func)

    def test_cbv_city_detail(self):
        response = self.client.get(reverse("city:city_detail", kwargs={'pk': self.city_A.pk}))
        self.assertEqual(200, response.status_code)
        self.assertTemplateUsed(response, template_name='cities/city_detail.html')
        self.assertEqual(cities_views.CityDetailView.as_view().__name__, response.resolver_match.func.__name__)

    def test_find_all_routes(self):
        graph = routes_views.get_graph()
        all_ways = list(routes_views.dfs_paths(graph, self.city_A.pk, self.city_E.pk))
        self.assertEqual(len(all_ways), 4)

    def test_valid_form(self):
        form_data = {'from_city': self.city_A.pk, 'to_city': self.city_E.pk,
                     'across_cities': [self.city_C.pk], 'traveling_time': 30}
        form = RouteForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_messages_error_more_time(self):
        response = self.client.post('/find/', {'from_city': self.city_A.pk, 'to_city': self.city_E.pk,
                                               'across_cities': [self.city_C.pk], 'traveling_time': 10})
        self.assertContains(response, 'Route time is longer then specified.', 1, 200)

    def test_messages_error_another_city(self):
        response = self.client.post('/find/', {'from_city': self.city_B.pk, 'to_city': self.city_E.pk,
                                               'across_cities': [self.city_C.pk], 'traveling_time': 30})
        self.assertContains(response, 'Route through these cities is not possible.', 1, 200)
