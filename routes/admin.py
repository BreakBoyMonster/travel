from django.contrib import admin
from routes.models import Route


class RouteAdmin(admin.ModelAdmin):
    list_display = ('name', 'from_city', 'to_city', 'travel_time')
    list_editable = ('travel_time',)

    class Meta:
        model = Route


admin.site.register(Route, RouteAdmin)
