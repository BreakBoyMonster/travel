from django.db import models
from django.core.exceptions import ValidationError
from cities.models import City


class Train(models.Model):
    number = models.CharField(max_length=150, unique=True, verbose_name='Train number')
    from_city = models.ForeignKey(City, on_delete=models.CASCADE,
                                  verbose_name='from where', related_name='from_where')
    to_city = models.ForeignKey(City, on_delete=models.CASCADE,
                                verbose_name='to where', related_name='to_where')
    travel_time = models.IntegerField(verbose_name='Travel time')

    class Meta:
        verbose_name = 'Train'
        verbose_name_plural = 'Trains'
        ordering = ['number', ]

    def __str__(self):
        return "Train № {} from {} to {}".format(self.number, self.from_city, self.to_city)

    def clean(self, *args, **kwargs):
        if self.from_city == self.to_city:
            raise ValidationError("Change destination city, please!")
        qs = Train.objects.filter(from_city=self.from_city,
                                  to_city=self.to_city,
                                  travel_time=self.travel_time).exclude(pk=self.pk)
        if qs.exists():
            raise ValidationError("Change travel time, please!")
        return super(Train, self).clean(*args, **kwargs)
