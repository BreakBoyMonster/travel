from trains.models import Train
from trains.forms import TrainForm
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.paginator import Paginator
from django.shortcuts import render
from django.urls import reverse_lazy


class TrainCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = Train
    form_class = TrainForm
    template_name = 'trains/train_create.html'
    success_url = reverse_lazy('train:home')
    success_message = 'Train added successfully!'
    login_url = "/login/"


class TrainUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Train
    form_class = TrainForm
    template_name = 'trains/train_update.html'
    success_url = reverse_lazy('train:home')
    success_message = 'Train edited successfully!'
    login_url = "/login/"


class TrainDeleteView(LoginRequiredMixin, DeleteView):
    model = Train
    success_url = reverse_lazy('train:home')
    login_url = "/login/"

    def get(self, request, *args, **kwargs):
        messages.success(request, 'Train deleted successfully!')  # for DeleteView it works like this
        return self.post(request, *args, **kwargs)


class TrainDetailView(DetailView):
    queryset = Train.objects.all()
    context_object_name = 'object'
    template_name = 'trains/train_detail.html'


def home(request):
    trains = Train.objects.all()
    paginator = Paginator(trains, 10)
    page = request.GET.get('page')
    trains = paginator.get_page(page)
    return render(request, 'trains/home.html', context={"objects_list": trains, })

