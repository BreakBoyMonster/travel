from django import forms
from trains.models import Train
from cities.models import City


# class HtmlForm(forms.Form):
#     name = forms.CharField(label='Train')


class TrainForm(forms.ModelForm):
    number = forms.CharField(label='Train',
                             widget=forms.TextInput(attrs={'class': 'form-control',
                                                           'placeholder': 'Enter train name', }))
    from_city = forms.ModelChoiceField(label='From', queryset=City.objects.all(),
                                       widget=forms.Select(attrs={'class': 'form-control',
                                                                  'placeholder': 'From'}))
    to_city = forms.ModelChoiceField(label='To', queryset=City.objects.all(),
                                     widget=forms.Select(attrs={'class': 'form-control',
                                                                'placeholder': 'To'}))
    travel_time = forms.IntegerField(label='Train',
                                     widget=forms.NumberInput(attrs={'class': 'form-control',
                                                                     'placeholder': 'Travel time'}))

    class Meta(object):
        model = Train
        fields = ('number', 'from_city', 'to_city', 'travel_time')
