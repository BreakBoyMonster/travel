from django.urls import path
from trains.views import home, TrainDetailView, TrainCreateView, TrainUpdateView, TrainDeleteView


urlpatterns = [
    path("train_detail/<int:pk>/", TrainDetailView.as_view(), name="train_detail"),
    path("train_create/", TrainCreateView.as_view(), name="train_create"),
    path("train_update/<int:pk>/", TrainUpdateView.as_view(), name="train_update"),
    path("train_delete/<int:pk>/", TrainDeleteView.as_view(), name="train_delete"),
    path("", home, name="home"),
]
