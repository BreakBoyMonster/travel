from django.contrib.messages.views import SuccessMessageMixin
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.paginator import Paginator
from django.shortcuts import render
from django.urls import reverse_lazy
from cities.models import City
from cities.forms import CityForm


class CityCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = City
    form_class = CityForm
    template_name = 'cities/city_create.html'
    success_url = reverse_lazy('city:home')
    success_message = 'City added successfully!'
    login_url = "/login/"


class CityUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = City
    form_class = CityForm
    template_name = 'cities/city_update.html'
    success_url = reverse_lazy('city:home')
    success_message = 'City edited successfully!'
    login_url = "/login/"


class CityDeleteView(LoginRequiredMixin, DeleteView):
    model = City
    # template_name = 'cities/city_delete.html'
    success_url = reverse_lazy('city:home')
    login_url = "/login/"

    def get(self, request, *args, **kwargs):
        messages.success(request, 'City deleted successfully!')  # for DeleteView it works like this
        return self.post(request, *args, **kwargs)


class CityDetailView(DetailView):
    queryset = City.objects.all()
    context_object_name = 'object'
    template_name = 'cities/city_detail.html'


def home(request):
    # # print(request.POST)
    # if request.method == 'POST':
    #     form = CityForm(request.POST or None)
    #     if form.is_valid():
    #         print(form.cleaned_data)
    # form = CityForm
    # # city = request.POST.get("name")
    # # print(city)
    cities = City.objects.all()
    paginator = Paginator(cities, 10)
    page = request.GET.get('page')
    cities = paginator.get_page(page)
    return render(request, 'cities/home.html', context={"objects_list": cities, })
