from django.urls import path
from cities.views import home, CityDetailView, CityCreateView, CityUpdateView, CityDeleteView

urlpatterns = [
    path("city_detail/<int:pk>/", CityDetailView.as_view(), name="city_detail"),
    path("city_create/", CityCreateView.as_view(), name="city_create"),
    path("city_update/<int:pk>/", CityUpdateView.as_view(), name="city_update"),
    path("city_delete/<int:pk>/", CityDeleteView.as_view(), name="city_delete"),
    path("", home, name="home"),
]
